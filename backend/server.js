import { createServer } from "http";
import app from "./app.js";

// Get port from environment and store in Express.
const port = process.env.PORT || 3000;
app.set("port", port);

// Create HTTP server.
const server = createServer(app);

// Listen on provided port.
server.listen(port, () => {
  console.log(`Your server is running on http://localhost:${port}`);
});
