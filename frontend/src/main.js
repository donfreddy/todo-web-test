import { createApp } from "vue";
import App from "./App.vue";
import mitt from "mitt";

import { Datetime } from "vue-datetime";

// Styles
import "@/assets/styles/tailwind.css";
import "vue-datetime/dist/vue-datetime.css";

const emitter = mitt();

const app = createApp(App);

app.config.globalProperties.emitter = emitter;

app.use(Datetime).mount("#app");
